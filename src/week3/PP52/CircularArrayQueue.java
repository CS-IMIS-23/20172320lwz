package src.week3.PP52;
import src.week2.EmptyCollectionException;
import src.week2.PP42.LinearNode;
import src.week3.QueueADT;

public class CircularArrayQueue<T> implements QueueADT<T> {
    private final int DEFAULT_CAPACITY = 100;
    private int front, rear, count;
    private T[] queue;

    public CircularArrayQueue(int initialCapacity){
        front = rear =count = 0;
        queue = ((T[])(new Object[initialCapacity]));

    }

    public CircularArrayQueue(){
        queue = ((T[])(new Object[DEFAULT_CAPACITY]));
    }

    @Override
    public void enqueue(T element) {
        if(size() == queue.length)
            expandCapacity();

        queue[rear] = element;
        rear = (rear+1)%queue.length;
        count++;
    }

    private void expandCapacity() {
        T[] larger = (T[]) (new Object[queue.length*2]);
        for(int scan = 0;scan <count; scan++){
            larger[scan] = queue[front];
            front = (front + 1) % queue.length;
        }
        front = 0;
        rear = count;
        queue = larger;
    }


    @Override
    public T dequeue() throws EmptyCollectionException {
        if(isEmpty())
            throw new EmptyCollectionException("queue");

        T result = queue[front];
        queue[rear] = null;
        front = (front + 1) % queue.length;

        count--;
        return result;

    }

    @Override
    public T first() {
        T result = queue[front];
        return result;
    }

    @Override
    public boolean isEmpty() {
        if (count==0)
            return true;
        else
            return false;
    }

    @Override
    public int size() {
            return count;
        }
    public String toString() {
        String result = "";
        int temp = front;
        for (int b = 0; b < size(); b++) {
            result += queue[b] + " ";
            temp = (temp+1)%queue.length;

        }
        return result;
    }
}

