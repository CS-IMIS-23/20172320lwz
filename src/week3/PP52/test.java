package src.week3.PP52;

public class test {
    public static void main(String[] args) {
        CircularArrayQueue circularArrayQueue = new CircularArrayQueue(2);
        circularArrayQueue.enqueue(5);
        circularArrayQueue.enqueue(6);
        circularArrayQueue.enqueue(3);
        System.out.println("队列元素："+circularArrayQueue.toString());
        circularArrayQueue.dequeue();
        System.out.println("删除后："+circularArrayQueue.toString());
        System.out.println("元素数目:"+circularArrayQueue.size());
        System.out.println("是否为空："+circularArrayQueue.isEmpty());
        System.out.println("队列前端元素:"+circularArrayQueue.first());

    }
}
