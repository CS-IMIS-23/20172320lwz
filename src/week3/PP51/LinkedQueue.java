package src.week3.PP51;

import src.week2.EmptyCollectionException;
import src.week2.PP42.LinearNode;
import src.week3.QueueADT;

public class LinkedQueue<T> implements QueueADT<T> {
    private int count;
    private LinearNode<T> head, tail;

    public LinkedQueue(){
        count = 0;
        head = tail = null;
}
    @Override
    public void enqueue(T element) {
        LinearNode<T> node = new LinearNode<T>(element);

        if(isEmpty())
            head = node;
        else
            tail.setNext(node);
        tail = node;
        count ++;
    }

    @Override
    public T dequeue() throws EnumConstantNotPresentException{
        if(isEmpty())
            throw new EmptyCollectionException("queue");

        T result = head.getElement();
        head = head.getNext();
        count --;

        if(isEmpty())
            tail = null;

        return result;
    }

    @Override
    public T first() {
        return head.getElement();
    }

    @Override
    public boolean isEmpty() {
        if (count==0)
            return true;
        else
            return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String toString() {
        String string = "";
        LinearNode<T> current = head;
        int x =count;
        while (x>0){
            string += current.getElement() + " ";
            current = current.getNext();
            x--;
        }
        return string;
    }
}
