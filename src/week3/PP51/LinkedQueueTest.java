package src.week3.PP51;

public class LinkedQueueTest {
    public static void main(String[] args) {
        LinkedQueue linkedQueue=new LinkedQueue();
        linkedQueue.enqueue(5);
        linkedQueue.enqueue(6);
        linkedQueue.enqueue(4);
        linkedQueue.enqueue(8);
        linkedQueue.enqueue(9);
        System.out.println("队列前端元素："+linkedQueue.first());
        System.out.println("队列是否为空："+linkedQueue.isEmpty());
        System.out.println("队列元素数量："+linkedQueue.size());
        System.out.println(linkedQueue.toString());
    }
}
