package src.week6;

import jdk.nashorn.api.tree.Tree;

import java.io.*;

/**
 * BackPainAnaylyzer demonstrates the use of a binary decision tree to 
 * diagnose back pain.
 */
public class BackPainAnalyzer
{
    /**
     *  Asks questions of the user to diagnose a medical problem.
     */
    public static void main (String[] args) throws FileNotFoundException
    {
        System.out.println ("So, you're having back pain.");

        DecisionTree expert = new DecisionTree("C:\\Users\\mac\\IdeaProjects\\20172320lwz\\src\\week6\\input.txt");
        expert.evaluate();
        System.out.println("叶子结点数："+expert.countLeaf());
        System.out.println("深度："+expert.Deep());
        System.out.println("非递归层序遍历：");
        expert.levelOrder();
    }
}
