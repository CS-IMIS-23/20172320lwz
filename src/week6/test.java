package src.week6;

public class test {
    public static void main(String[] args) {
        LinkedBinaryTree<String> linkedBinaryTree1 = new LinkedBinaryTree<>("2320");
        LinkedBinaryTree<String> linkedBinaryTree2 = new LinkedBinaryTree<>("LWZ");
        LinkedBinaryTree<String> linkedBinaryTree3 = new LinkedBinaryTree<>("overwatch");
        System.out.println("是否为空："+linkedBinaryTree3.isEmpty());
        LinkedBinaryTree linkedBinaryTree4= new LinkedBinaryTree("wz",linkedBinaryTree1,linkedBinaryTree2);
        LinkedBinaryTree linkedBinaryTree5 = new LinkedBinaryTree("dky",linkedBinaryTree4,linkedBinaryTree1);
        System.out.println();
        linkedBinaryTree5.toPreString();
        System.out.println(" ");
        System.out.println("2320是否为内部结点:"+linkedBinaryTree2.getRootNode().isLeaf());
        System.out.println("删除所有结点：");
        linkedBinaryTree5.removeElement();
        System.out.println(linkedBinaryTree5.toString());

    }
}
