package src.week6;


import java.util.*;
import java.io.*;

/**
 * The DecisionTree class uses the LinkedBinaryTree class to implement 
 * a binary decision tree. Tree elements are read from a given file and  
 * then the decision tree can be evaluated based on user input using the
 * evaluate method. 
 * 
 * @author Lewis and Chase
 * @version 4.0
 */
public class DecisionTree
{
    private LinkedBinaryTree<String> tree;
    
    /**
     * Builds the decision tree based on the contents of the given file
     *
     * @param filename the name of the input file
     * @throws FileNotFoundException if the input file is not found
     */
    public DecisionTree(String filename) throws FileNotFoundException
    {
        File inputFile = new File(filename);
        Scanner scan = new Scanner(inputFile);
        int numberNodes = scan.nextInt();
        scan.nextLine();
        int root = 0, left, right;
        
        List<LinkedBinaryTree<String>> nodes = new java.util.ArrayList<LinkedBinaryTree<String>>();
        for (int i = 0; i < numberNodes; i++)
            nodes.add(i,new LinkedBinaryTree<String>(scan.nextLine()));
        
        while (scan.hasNext())
        {
            root = scan.nextInt();
            left = scan.nextInt();
            right = scan.nextInt();
            scan.nextLine();
            
            nodes.set(root, new LinkedBinaryTree<String>((nodes.get(root)).getRootElement(), 
                                                       nodes.get(left), nodes.get(right)));
        }
        tree = nodes.get(root);
    }

    /**
     *  Follows the decision tree based on user responses.
     */
    public void evaluate()
    {
        LinkedBinaryTree<String> current = tree;
        Scanner scan = new Scanner(System.in);

        while (current.size() > 1)
        {
            System.out.println (current.getRootElement());
            if (scan.nextLine().equalsIgnoreCase("N")) {
                current = current.getLeft();
            } else {
                current = current.getRight();
            }
        }

        System.out.println (current.getRootElement());
    }
    public int CountLeaf(BinaryTreeNode node){
        if (node!=null){
            if (node.getLeft()==null&&node.getRight()==null){
                return 1;
            }
            return CountLeaf(node.getLeft())+CountLeaf(node.getRight());
        }
        return 0;
    }
    public int deep(BinaryTreeNode node){
    int h1,h2;
    if (node==null){
        return 0;
    }
    else {
        h1=deep(node.left);
        h2=deep(node.right);
        return (h1<h2)?h2+1:h1+1;
    }
    }
    public int countLeaf(){
        BinaryTreeNode binaryTreeNode=tree.root;
        return CountLeaf(binaryTreeNode);
    }
    public  int Deep(){
        BinaryTreeNode binaryTreeNode=tree.root;
        return deep(binaryTreeNode);
    }
    public void levelOrder(){
        BinaryTreeNode node=tree.root;
        if (node==null){
            return;
        }
        BinaryTreeNode binaryTreeNode;
        Queue<BinaryTreeNode> queue=new LinkedList<>();
        queue.add(node);
        while (queue.size()!=0){
            binaryTreeNode = queue.poll();

            System.out.println(binaryTreeNode.element+" ");
            if (binaryTreeNode.left!=null){
                queue.offer(binaryTreeNode.right);
            }
        }
    }

}