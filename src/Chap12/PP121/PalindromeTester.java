package src.Chap12.PP121;

import java.util.Scanner;

public class PalindromeTester {
    public static void judge(String str){
        int left,right;
        left=0;
        right=str.length()-1;

        while (str.charAt(left)==str.charAt(right)&&left<right){
            left++;
            right--;

        }
        if (left<right)
            System.out.println("That string is NOT a palindrome.");
        else
            System.out.println("That string IS a palindrome.");
    }
    public static void main(String[] args) {
        String str,another="y";
        int left,right;
        PalindromeTester result=new PalindromeTester();
        Scanner scan=new Scanner(System.in);

        while (another.equalsIgnoreCase("y")){
            System.out.println("Enter a potential palindrome:");
            str=scan.nextLine();
            System.out.println();
            judge(str);

            System.out.println();
            System.out.println("Test another palindrome(y/n)?");
            another=scan.nextLine();
        }
    }

}
