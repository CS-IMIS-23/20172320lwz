package src.Chap12;

import java.io.*;
import java.util.Scanner;

public class Fn {
    public static void main(String[] args) throws IOException {

        File file=new File("D://wz","Fn.txt");

        FileWriter outputStream = new FileWriter(file);

        Scanner scan = new Scanner(System.in);
        int n;
        System.out.println("请输入一个整数n：");
        n = scan.nextInt();
        int result = fact(n);
        System.out.println("函数F(n)=" + result);

        outputStream.write(" "+ result);
        outputStream.flush();


    }

    static int fact(int n){
        int result;
        if (n==0)
            return 0;
        else
        if (n==1)
            return 1;
        else
            result=fact(n-1)+fact(n-2);
        return result;
    }


}