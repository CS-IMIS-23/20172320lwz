package src.Chap12;

import java.util.Scanner;

public class Pascal {
    public static void main(String[] args) {
        int num;
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入三角形的行数：");
        num = scanner.nextInt();

        int[][] value;
        value=new int[num][num];
        for (int i = 0; i < num; i++) {
            value[i][0] = 1;
            value[i][i] = 1;
            if (i > 1) {
                for (int j = 1; j < i; j++) {
                    value[i][j] = value[i - 1][j - 1] + value[i - 1][j];

                }
            }
        }

        for (int i = 0; i < num; i++) {

                System.out.print("\n");

            for (int j = 0; j <= i; j++) {
                System.out.print(" "+value[i][j] + " ");
            }
        }


    }
}


