package src.test;

public class Complex {
    public double getRealPart() {
        return RealPart;
    }

    public double getImagePart() {
        return ImagePart;
    }

    double RealPart;
    double ImagePart;

    public void setRealPart(double realPart) {
        RealPart = realPart;
    }

    public void setImagePart(double imagePart) {
        ImagePart = imagePart;
    }

    public Complex(){}
    public Complex(double R,double I){
        this.ImagePart = I;
        this.RealPart = R;
    }


    @Override
    public String toString() {
        return "Complex{" +
                "RealPart=" + RealPart +
                ", ImagePart=" + ImagePart +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Complex complex = (Complex) o;
        return Double.compare(complex.getRealPart(), getRealPart()) == 0 &&
                Double.compare(complex.getImagePart(), getImagePart()) == 0;
    }

    public Complex ComplexAdd(Complex a) {
        return new Complex(this.RealPart + a.RealPart,this.ImagePart + a.ImagePart);
    }
    public Complex ComplexSub(Complex a){
        return new Complex(this.RealPart - a.RealPart,this.ImagePart - a.ImagePart);
    }
    public Complex ComplexMulti(Complex a){
        return new Complex(this.RealPart*a.RealPart-this.ImagePart*a.ImagePart,
                this.ImagePart*a.RealPart+this.RealPart*a.ImagePart);
    }
    public Complex ComplexDiv(Complex a){
        double scale = a.getRealPart()*a.getRealPart() + a.getImagePart()*a.getImagePart();
        Complex b = new Complex(a.getRealPart() / scale, - a.getImagePart() / scale);
        return this.ComplexMulti(b);
    }
}