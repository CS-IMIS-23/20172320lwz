package src.Chap11;

import src.Chap10.Sorting;

import java.io.*;
import java.util.Scanner;

public class IOTest {
    public static void main(String[] args) throws IOException {
        //创建文件
        File file = new File("D:\\wz", "sort.txt");
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException Exception) {
            System.out.println("error");
        }
       //写入文件
        FileWriter fileWriter=new FileWriter(file);
        Scanner scanner=new Scanner(System.in);
        String str;
        System.out.println("请输入数字：");
        str=scanner.nextLine();
        String[] numstr=str.split(" ");
        int[] nums=new int[numstr.length];
        for (int i=0;i<numstr.length;i++){
            nums[i]=Integer.parseInt(numstr[i]);
        }
        for (int n: nums){
            System.out.println(n);
        }
        fileWriter.write(""+str);
        fileWriter.flush();
        System.out.println();

        //排序

        Sorting.selectionSort(numstr);

        for (String n:numstr) {
            System.out.println(n);
            //排序结果写入文件
            fileWriter.write("  " + n);
            fileWriter.flush();
        }
    }
}