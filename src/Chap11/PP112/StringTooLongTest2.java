package src.Chap11.PP112;

import src.Chap11.PP111.StringTooLongException;

import java.util.Scanner;

    public class StringTooLongTest2 {
        public static void main(String[] args) throws StringTooLongException {
            Scanner scanner=new Scanner(System.in);
            String string = "";
            StringTooLongException problem=new StringTooLongException();
            System.out.println("请输入一串字符");
            try {
                string=scanner.nextLine();
                if (string.length()>20)
                    throw problem;
            }
            catch (StringTooLongException p){
                System.out.println(p.toString());
            }
            if (!string.equals("DONE"))
                System.out.println("字符串结束");
        }
    }


