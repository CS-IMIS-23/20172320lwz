package src.Chap11.PP111;

import java.util.Scanner;

public class StringTooLongTest {
    public static void main(String[] args) throws StringTooLongException{
        Scanner scanner=new Scanner(System.in);
        String string = "";
        StringTooLongException problem=new StringTooLongException();
        System.out.println("请输入一串字符");
        try {
            string=scanner.nextLine();
            if (string.length()>20)
                throw problem;
        }
        catch (StringTooLongException p){
            System.out.println(p.toString());
        }

    }
}
