package src.week2.PP42;

public interface  StackADT<T> {
    public void push (T element);
    public T pop();
    public T peek();
    public boolean isEmpty();
    public int size();
    public String toString();
}
