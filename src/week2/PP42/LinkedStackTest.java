package src.week2.PP42;

public class LinkedStackTest {
    public static void main(String[] args) {
        LinkedStack linkedStack = new LinkedStack();
        linkedStack.push("2017");
        linkedStack.push("2320");
        linkedStack.push("wz");

        System.out.println("元素数目："+linkedStack.size());
        System.out.println("是否为空："+linkedStack.isEmpty());
        System.out.println(linkedStack.toString());
    }
}
