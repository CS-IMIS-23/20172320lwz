package src.week2.PP42;
import src.week2.EmptyCollectionException;
public class LinkedStack<T> implements StackADT {
    private int count;
    private LinearNode<T> top;

    public LinkedStack() {
        count = 0;
        top = null;
    }

    @Override
    public void push(Object element) {
        LinearNode<T> temp = new LinearNode<T>((T) element);
        temp.setNext(top);
        top = temp;
        count++;
    }

    @Override
    public Object pop() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("Stack");
        T result = top.getElement();
        top = top.getNext();
        count--;
        return result;
    }

    @Override
    public Object peek() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("Stack");
        T temp = top.getElement();
        return temp;
    }

    @Override
    public boolean isEmpty() {
        if (size() == 0)
            return true;
        else
            return false;
    }

    @Override
    public int size() {
        return count;
    }
    public String toString(){
        String string = "";
        LinearNode<T> current = new LinearNode<>();
        current = top;
        int x =count;
        while (x>0){
            string += current.getElement() + " ";
            current = current.getNext();
            x--;
        }
        return string;
    }
}


