package src.week2;

public class ArrayStackTest {
    public static void main(String[] args) {
        ArrayStack arrayStack=new ArrayStack();
        arrayStack.push("2017");
        arrayStack.push("2320");
        arrayStack.push("wz");

        System.out.println("顶端元素："+ arrayStack.peek());
        System.out.println("是否为空："+ arrayStack.isEmpty());
        System.out.println("元素数目："+ arrayStack.size());
        System.out.println(arrayStack.toString());
    }
}
