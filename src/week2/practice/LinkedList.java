package src.week2;

public class LinkedList {
    public static void main(String[] args) {
        Integer integer = new Integer(1);
        Integer Head = integer;
        Integer integer1 = new Integer(5);
        Integer integer2 = new Integer(8);
        Integer integer3 = new Integer(9);
        Integer integer4 = new Integer(7);
        Integer integer5 = new Integer(2);
        Integer integer6 = new Integer(3);
        Integer integer7 = new Integer(4);
        PrintLinkedList(Head);
        InsertNode(Head, integer1);
        InsertNode(Head, integer2);
        InsertNode(Head, integer3);
        InsertNode(Head, integer4);
        InsertNode(Head, integer5);
        InsertNode(Head, integer6);
        InsertNode(Head, integer7);
        System.out.println(" ");
        System.out.println("插入后：");
        PrintLinkedList(Head);
        DeleteNode(Head, integer4);
        DeleteNode(Head, integer);
        System.out.println(" ");
        System.out.println("删除后：");
        PrintLinkedList(Head);
        selectSorting(Head);
        System.out.println("排序后：");
        PrintLinkedList(Head);
    }

    //输出
    public static void PrintLinkedList(Integer Head) {
        Integer node = Head;
        while (node != null) {
            System.out.println(node.number);
            node = node.next;
        }
    }

    //插入
    public static void InsertNode(Integer Head, Integer node) {
        Integer temp = Head;
        while (temp.next != null) {
            temp = temp.next;
        }
        temp.next = node;
    }

    //删除
    public static void DeleteNode(Integer Head, Integer node) {
        Integer PreNode = Head, CurrentNode = Head;
        while (CurrentNode != null) {
            if (CurrentNode.number != node.number) {
                PreNode = CurrentNode;
                CurrentNode = CurrentNode.next;
            } else {
                break;
            }
        }
        PreNode.next = CurrentNode.next;
    }

    //排序
    public static Integer selectSorting(Integer Head) {
        Integer PreNode = Head, CurrentNode = null;
        if (Head == null || Head.next == null) {
            return Head;
        }
        while (PreNode.next != CurrentNode){
        while (PreNode.next != CurrentNode) {
            if (PreNode.number > PreNode.next.number) {
                int temp = PreNode.number;
                PreNode.number = PreNode.next.number;
                PreNode.next.number = temp;
            }
            PreNode = PreNode.next;
        }
        CurrentNode = PreNode;
        PreNode = Head;
        }
        return Head;
    }
}




//    public static void selectionSort(Comparable[] list)
//    {
//        int min;
//        Comparable temp;
//
//        for (int index = 0; index < list.length-1; index++)
//        {
//            min = index;
//            for (int scan = index+1; scan < list.length; scan++)
//                if (list[scan].compareTo(list[min]) < 0)
//                    min = scan;
//
//            temp = list[min];
//            list[min] = list[index];
//            list[index] = temp;
//        }
//    }

