package src.week2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Stack;

public class reverse {
    public static void main(String[] args) {
        String string, result = "";
        Stack stack = new Stack();
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入字符串：");
        string = scanner.nextLine();
        System.out.println("反序后：" + reverse(string));
        scanner.close();
    }

    public static String reverse(String string) {
        char[] chars = string.toCharArray();
        string = "";
        for (int x = chars.length - 1; x >= 0; x--) {
            string += chars[x];
        }
        return string;

    }
}


