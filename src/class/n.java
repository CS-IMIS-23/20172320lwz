//-----------------------------------------------------------------------------
//n.java                                Author:wz
//
//This is today's class practice
//-----------------------------------------------------------------------------
import java.util.Scanner;
public class n
{
public static void main(String[] args)
{
int n;
Scanner scan = new Scanner(System.in);
System.out.println("Enter the number: ");
n = scan.nextInt();
int x = 1;
//for loop
for(int count = 1;count<=n;count++)
{
x = x*count;
}
System.out.println("Result: " +x);

//while loop
int y = 1;
while(n !=1)
{
y = y*n;
n--;
}
System.out.println("Result: "+y);
}
}
