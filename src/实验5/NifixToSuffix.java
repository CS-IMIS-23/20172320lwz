package src.实验5;
import java.util.Stack;
import java.util.StringTokenizer;

public class NifixToSuffix
{
    private int number, num1, num2;
    private int value1, value2;
    private int index = 0;
    private int[][] array = {{0, 0, 0, 0, 0},
            {0, 1, 1, -1, -1},
            {0, 1, 1, -1, -1},
            {0, 1, 1, 1, 1},
            {0, 1, 1, 1, 1}};
    Stack stack = new Stack();

    public NifixToSuffix() {
        num1 = 0;
        num2 = 0;
        value1 = 0;
        value2 = 0;
    }

    public String getAnswer(String obj) {
        String result = "";
        StringTokenizer st = new StringTokenizer(obj);
        while (st.hasMoreTokens()) {
            String m = st.nextToken();

            if (m.length() > 1)
                result += m + " ";
            else {
                char[] ar = m.toCharArray();
                char x = ar[0];

                if ((x >= '0' && x <= '9') || x == ' ')
                    result += x + " ";

                else {
                    switch (x) {
                        case '+':
                            number = 1;
                            break;
                        case '-':
                            number = 2;
                            break;
                        case '*':
                            number = 3;
                            break;
                        case '/':
                            number = 4;
                            break;
                    }
                    if (stack.empty()) {
                        num1 = number;
                        number = 0;
                    } else {
                        num2 = number;
                        number = 0;
                    }
                    if (array[num1][num2] <= 0) {
                        stack.push(x);
                        value1 += 1;
                    } else {
                        result += stack.pop() + " ";
                        if(!stack.empty()) {
                            if (String.valueOf(stack.peek()).equals("+") || String.valueOf(stack.peek()).equals("-")) {
                                result += stack.pop() + " ";
                                value1 -= 1;
                            }
                        }
                        stack.push(x);
                    }
                }
            }
        }
        for (int y = 0; y < value1; y++)
            result += stack.pop() + " ";

        return result;
    }
}


