package src.实验5;


import src.test5.Questions;
import src.test5.suffixConvertor;

import java.math.*;
import java.security.spec.*;
import javax.crypto.spec.*;
import javax.crypto.interfaces.*;
import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by besti on 2018/6/9.
 */
public class SocketClient {
    public static void main(String[] args) throws IOException, NoSuchPaddingException, NoSuchAlgorithmException, ClassNotFoundException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        //1.建立客户端Socket连接，指定服务器位置和端口
//        Socket socket = new Socket("localhost",8080);
        Socket socket = new Socket("192.168.1.106",8800);

        //2.得到socket读写流
        OutputStream outputStream = socket.getOutputStream();
        //       PrintWriter printWriter = new PrintWriter(outputStream);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        //输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //3.利用流按照一定的操作，对socket进行读写操作
//        String info1 = "12 15 8 100 25 34 19";
        String expression=null;
        suffixConvertor sc=new suffixConvertor();

        Caculator caculator=new Caculator();
        Questions Iq=new Questions();
        Random random=new Random();

        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入中缀表达式(每个字符之间要有空格：");
        expression=scanner.nextLine();
        //中缀转后缀
        NifixToSuffix nifixToSuffix = new NifixToSuffix();
        String  res = nifixToSuffix.getAnswer(expression);
        System.out.println("后缀表达式为："+ res );
        String s = new String(res.getBytes("GBK"),"utf-8");

        // 读取对方的DH公钥
        FileInputStream f1=new FileInputStream("key2_pub.dat");
        ObjectInputStream b1=new ObjectInputStream(f1);
        PublicKey  pbk=(PublicKey)b1.readObject( );
        //读取自己的DH私钥
        FileInputStream f2=new FileInputStream("key1_pri.dat");
        ObjectInputStream b2=new ObjectInputStream(f2);
        PrivateKey  prk=(PrivateKey)b2.readObject( );
        // 执行密钥协定
        KeyAgreement ka=KeyAgreement.getInstance("DH");
        ka.init(prk);
        ka.doPhase(pbk,true);
        //生成共享信息
        byte[ ] sb=ka.generateSecret();
        System.out.println("客户端密钥：");
        for(int i=0;i<sb.length;i++) {
            System.out.print(sb[i] + ",");
        }
        SecretKeySpec k=new SecretKeySpec(sb,0,24,"AES");

        Cipher cp=Cipher.getInstance("AES");
        cp.init(Cipher.ENCRYPT_MODE, k);
        System.out.print("字节数组编码：");
        byte ptext[]=s.getBytes("UTF8");
        for(int i=0;i<ptext.length;i++){
            System.out.print( + ptext[i]+",");
        }
        System.out.println("");
        System.out.print("加密后的内容: ");
        byte ctext[]=cp.doFinal(ptext);
        for(int i=0;i<ctext.length;i++){
            System.out.print( + ctext[i] +",");
        }
        FileOutputStream f3=new FileOutputStream("SEnc.dat");
        f3.write(ctext);


        //将密文进行传输
        outputStream.write(ctext);
        outputStream.flush();
        socket.shutdownOutput();
        //接收服务器的响应
        String reply = null;
        while (!((reply = bufferedReader.readLine()) ==null)){
            System.out.println("\n接收服务器的信息为：" + reply);
        }
        //4.关闭资源
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        //printWriter.close();
        outputStream.close();
        socket.close();
    }
}
