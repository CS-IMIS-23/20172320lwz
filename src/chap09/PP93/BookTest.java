package src.chap09.PP93;

public class BookTest {
    public static void main(String[] args) {
        //杂志
        dictionary magzine=new dictionary(100,"电影介绍","《看电影》","",50000);
        System.out.println("Type: 杂志");

        System.out.println("Keywords: "+magzine.getKeywords());

        System.out.println("Name: "+magzine.getBookname());

        System.out.println("Definitions per page: "+magzine.computeRatio());

       //小说
        dictionary novel=new dictionary(250,"网络小说","《斗罗大陆》","唐家三少",60000);

        System.out.println("Type: 小说");

        System.out.println("Keywords: "+novel.getKeywords());

        System.out.println("Name: "+novel.getBookname());

        System.out.println("Author: "+novel.getAuthor());

        System.out.println("Definitions per page: "+novel.computeRatio());
        System.out.println();

        //学术刊物
        dictionary academic=new dictionary(250,"Java","《如何学好Java》","王志强",50000);
        System.out.println("Type: 学术刊物");

        System.out.println("Keywords: "+academic.getKeywords());

        System.out.println("Name: "+academic.getBookname());

        System.out.println("Author: "+academic.getAuthor());

        System.out.println("Definitions per page: "+academic.computeRatio());
        System.out.println();

        //课本
        dictionary textbook=new dictionary(480,"java","《Java程序设计教程》","John Lewis",50000);
        System.out.println("Type: 课本");

        System.out.println("Keywords: "+textbook.getKeywords());

        System.out.println("Name: "+textbook.getBookname());

        System.out.println("Author: "+textbook.getAuthor());

        System.out.println("Definitions per page: "+textbook.computeRatio());
    }
}
