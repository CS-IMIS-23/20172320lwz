package src.chap09.PP93;

public class Book3 {
    protected int pages;
    protected String keywords;
    protected String bookname;
    protected String author;


    public Book3(int pages,String keywords,String bookname,String author){
        this.pages=pages;
        this.keywords=keywords;
        this.bookname=bookname;
        this.author=author;

    }

    public void setPages(int numpages){
        pages=numpages;
    }
    public int getPages(){
        return pages;
    }

    public void setKeywords(String key){
        keywords=key;
    }

    public String getKeywords(){
        return keywords;
    }
    public void setBookname(String name){
        bookname=name;
    }

    public String getBookname() {
        return bookname;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthor() {
        return author;
    }
}
