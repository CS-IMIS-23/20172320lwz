package src.chap09.PP93;

public class dictionary extends Book3 {
    private int definition;
    public dictionary(int numpages,String key,String name,String author,int numDefinition){
        super(numpages,key,name,author);

        definition=numDefinition;
    }
    public double computeRatio(){
        return (double) definition/pages;
    }

    public void setDefinition(int numDefinition){
        definition=numDefinition;
    }

    public int getDefinition(){
        return definition;
    }

}
