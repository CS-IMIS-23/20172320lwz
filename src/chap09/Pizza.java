package src.chap09;
//----------------------------------------------------------------------
//Pizza.java                            Author:wz
//
//Represents a pizza, wihch is a food item. Used to demonstrate
// indirct referencing through inheritance.
//----------------------------------------------------------------------
public class Pizza extends FoodItem{
    //---------------------------------------------------------------------
    //Sets up a pizza wiy=th the specified amount of fat(assumes
    //eight servings).
    //----------------------------------------------------------------------
    public Pizza(int fatGrams)
    {
        super(fatGrams,8);
    }
}
