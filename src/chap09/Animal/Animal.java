package src.chap09.Animal;

public abstract class Animal {
    public String name;
    public int id;

    public Animal(String name, int id) {
        this.name = name;
        this.id = id;
    }
    public abstract void eat();
    public abstract void sleep();
    public abstract void introduction();
}
