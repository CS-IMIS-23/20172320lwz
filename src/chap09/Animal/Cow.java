package src.chap09.Animal;

import src.chap09.Animal.Animal;

public class Cow extends Animal {
    public Cow(String name, int id) {
        super(name, id);
    }

    @Override
    public void eat() {

        System.out.println("正在吃");
    }

    @Override
    public void sleep() {
        System.out.println("正在睡");
    }

    @Override
    public void introduction() {
        System.out.println("大家好，我是一头牛，编号"+id+"名字是"+name);
    }
}
