package src.chap09.Animal;

public class AnimalTest {
    public static void main(String[] args) {
        Cow cow=new Cow("阿俊",20172333);
        Sheep sheep=new Sheep("阿楠",20172330);

        cow.introduction();
        cow.eat();
        cow.sleep();
        System.out.println();

        sheep.introduction();
        sheep.eat();
        sheep.sleep();
    }
}
