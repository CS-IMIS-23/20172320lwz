package src.chap09.Animal;

import src.chap09.Animal.Animal;

public class Sheep extends Animal {
    public Sheep(String name, int id) {
        super(name, id);
    }

    @Override
    public void eat() {
        System.out.println("正在吃");
    }

    @Override
    public void sleep() {
        System.out.println("正在睡觉");
    }

    @Override
    public void introduction() {
        System.out.println("大家好，我是一只羊，编号"+id+"名字是"+name);
    }
}
