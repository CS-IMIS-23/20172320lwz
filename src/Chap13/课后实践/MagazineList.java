package src.Chap13.课后实践;//*******************************************************************
//  MagazineList.java       Author: Lewis/Loftus
//
//  Represents a collection of magazines.
//*******************************************************************


import src.Chap13.课后实践.Magazine;

public class MagazineList
{
   private MagazineNode list;

   //----------------------------------------------------------------
   //  Sets up an initially empty list of magazines.
   //----------------------------------------------------------------
   public MagazineList()
   {
      list = null;
   }

   //----------------------------------------------------------------
   //  Creates a new MagazineNode object and adds it to the end of
   //  the linked list.
   //----------------------------------------------------------------
   public void add(Magazine mag)
   {
      MagazineNode node = new MagazineNode(mag);
      MagazineNode current;

      if (list == null)
         list = node;
      else
      {
         current = list;
         while (current.next != null)
            current = current.next;
         current.next = node;
      }
   }
   public void insert(int index,Magazine newmagazine) {
      MagazineNode magazineNode = new MagazineNode(newmagazine);
      MagazineNode node=list;
      for (int i=0;i<index-2;i++){
         node=node.next;
      }
      magazineNode.next=node.next;
      node.next=magazineNode;

   }

   public void delete(Magazine delNode,int index){
      MagazineNode node=list;
      int j=0;
      while (node!=null&&j<index-2){
         node=node.next;
         j++;
      }
      node.next=node.next.next;
   }




   //----------------------------------------------------------------
   //  Returns this list of magazines as a string.
   //----------------------------------------------------------------
   public String toString()
   {
      String result = "";

      MagazineNode current = list;

      while (current != null)
      {
         result += current.magazine + "\n";
         current = current.next;
      }

      return result;
   }

   //*****************************************************************
   //  An inner class that represents a node in the magazine list.
   //  The public variables are accessed by the MagazineList class.
   //*****************************************************************
   private class MagazineNode
   {
      public Magazine magazine;
      public MagazineNode next;

      //--------------------------------------------------------------
      //  Sets up the node
      //--------------------------------------------------------------
      public MagazineNode(Magazine mag)
      {
         magazine = mag;
         next = null;
      }
   }
}
