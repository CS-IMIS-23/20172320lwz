package src.Chap13.课后实践;

import src.Chap13.课后实践.Magazine;
import src.Chap13.课后实践.MagazineList;

public class MagazineRack
{
   //----------------------------------------------------------------
   //  Creates a MagazineList object, adds several magazines to the
   //  list, then prints it.
   //----------------------------------------------------------------
   public static void main(String[] args)
   {
      MagazineList rack = new MagazineList();
      Magazine magazine=new Magazine("Magazine");
      Magazine newmagezine=new Magazine("newmagazine");

      rack.add(new Magazine("Time"));
      rack.add(new Magazine("Woodworking Today"));
      rack.add(new Magazine("Communications of the ACM"));
      rack.add(new Magazine("House and Garden"));
      rack.add(new Magazine("GQ"));

      System.out.println(rack);
      //插入两个后
      rack.insert(0,newmagezine);
      rack.insert(6,magazine);
      System.out.println("插入后：\n"+rack);

      //删除节点
      rack.delete(newmagezine,0);
      rack.delete(magazine,5);
      System.out.println("删除后：\n"+rack);
   }
}
