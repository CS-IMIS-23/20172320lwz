package src.Chap13.PP131;

import src.chap08.DVD;

import java.text.NumberFormat;
import java.util.LinkedList;

public class DVDList {
    private LinkedList<DVD> list;
    private int count;
    private double totalCost;


    public DVDList(){
        list=new LinkedList<>();
        count=0;
        totalCost = 0.0;
    }


    public void addDVD(String title, String director, int year, double cost, boolean bluray)
    {


        list.add(new DVD(title,director,year,cost,bluray)) ;
        totalCost+=cost;
        count++;
    }


    @Override
    public String toString()
    {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();

        String report = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
        report+="My DVD collection\n\n";

        report+="Number of DVDs: "+count+"\n";
        report+="Total cost: "+fmt.format(totalCost)+"\n";
        report+="Average cost: "+fmt.format(totalCost/count);

        report+="\n\nDVD List:\n\n";

        for (int dvd = 0;dvd<count;dvd++) {
            report+=list.get(dvd).toString()+"\n";
        }

        return report;
    }

//    private void increaseSize()
//    {
//        DVD[] temp = new DVD[colletion.length*2];
//
//        for (int dvd = 0;dvd<colletion.length;dvd++)
//            temp[dvd]=colletion[dvd];
//    }
}

