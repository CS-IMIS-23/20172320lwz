package src.Chap10.PP105;

import java.text.NumberFormat;

public class DVD2 implements Comparable
{
    private String title, director;
    private int year;
    private double cost;
    private boolean bluray;

    //--------------------------------------------------\
    //Creates a new DVD with the specified information.
    //---------------------------------------------------
    public DVD2(String title, String director, int year,double cost, boolean bluray)
    {
        this.title=title;
        this.director=director;
        this.year= year;
        this.cost=cost;
        this.bluray=bluray;
    }

    //------------------------------------------------------------------
    //Returns a string description of this DVD.
    //------------------------------------------------------------------

    public String toString() {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();
        String description;

        description = fmt.format(cost) + "\t" + year + "\t";
        description += title+"\t"+director;
        if (bluray)
            description +="\t"+"Blu-ray";

        return description;
    }

    public String getTitle() {
        return title;
    }

    public String getDirector() {
        return director;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
    public int compareTo(Object obj){
        int result;
        String Director=((DVD2)obj).getDirector();
        String Title=((DVD2)obj).getTitle();
        if (title.equals(Title))
            result=director.compareTo(Director);
        else
            result=title.compareTo(Title);
        return result;

    }
}

