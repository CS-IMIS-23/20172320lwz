package src.Chap10.PP105;

import src.Chap10.PP104.Sorting2;
import src.Chap10.Sorting;
//按电影名字母顺序排列
public class Movies2 {
    public static void main(String[] args) {
        DVD2[] movies = new DVD2[3];


        movies[0]= new DVD2("The Godfather", "Francis Ford Coppola", 1972, 200.00, true);
        movies[1]=new DVD2("Districy 9", "Neill Blomkamp", 2009, 300.00, false);
        movies[2]=new DVD2("SwordArt Online", "WZ", 2012, 100.00, true);
        Sorting.selectionSort(movies);
        for (DVD2 movie : movies)
            System.out.println(movie);

        }
}