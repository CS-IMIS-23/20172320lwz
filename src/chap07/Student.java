public class Student
{
private String firstname, lastname;
private Address homeAddress, schoolAddress;

public Student(String first, String last, Address home, Address school)
{
firstname = first;
lastname = last;
homeAddress = home;
schoolAddress = school;
}

public String toString()
{
String result;

result = firstname + " " + lastname + "\n";
result += "Home Address:\n" + homeAddress + "\n";
result += "School Address:\n" + schoolAddress;

return result;
}
}
