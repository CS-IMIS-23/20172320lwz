package src.chap04;

//-----------------------------------------------------------------------------
//   Car.java                             Author:wz
//-----------------------------------------------------------------------------

public class Car
{
    private String tradename;
    private String type;
    private int yearofthefactory;


    public Car(String name, String tp, int year)
    {
        setTradename(name);
        setType(tp);
        yearofthefactory = year;
    }



    public String getName()
    {
        return getTradename();
    }

    public String getTp()
    {
        return getType();
    }

    public int getYear()
    {
        return yearofthefactory;
    }

    @Override
    public String toString()
    {
        return"Trade Name: " + getTradename() + "Type: " + getType() + "Year of the Factory: " + yearofthefactory;
    }
    private boolean year1;
    int years;
    private final int now = 2018;
    public int years()
    {
        years = getNow() - yearofthefactory;
        return years;
    }
    public boolean isAntique()
    {

        if (years>45){
            year1 = true;}
        else{
            year1 = false;}
        return year1;
    }

    public String getTradename() {
        return tradename;
    }

    public void setTradename(String tradename) {
        this.tradename = tradename;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNow() {
        return now;
    }
}



