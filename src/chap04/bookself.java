package src.chap04;

//-----------------------------------------------------------------------------
//    bookself.java                          Author:wz
//    Date:2018.03.28
//-----------------------------------------------------------------------------

public class bookself
{
    public static void main(String[] args)
    {

        PP47 book;
        book = new PP47("Sword Art Online","Kawahara Reki","Dengeki Bunko","2009.04.10");

        System.out.println("Name: " + book.getBn());
        System.out.println("Author: " + book.getAu());
        System.out.println("Press: " + book.getPr());
        System.out.println("Copyrightdate: " + book.getDate());
    }
}

