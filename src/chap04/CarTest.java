package src.chap04;

//-----------------------------------------------------------------------------
//  CarTest.java                         Author:wz
//-----------------------------------------------------------------------------

public class CarTest
{
    public static void main(String[] args)
    {
        Car car;
        int year=1990;
        car = new Car("WZ factory","Porsche",year);

        System.out.println("Trade Name: " + car.getName());
        System.out.println("Type: " + car.getTp());
        System.out.println("Year: " + car.getYear());
        System.out.println("Is antique or not: " +car.isAntique());
    }
}
