package src.chap04;

//-----------------------------------------------------------------------------
// book.java                    Author:wz
//  Date:2018.03.28
//-----------------------------------------------------------------------------
public class PP47
{
    private String bookname;
    private String author;
    private String press;
    private String copyrightdate;


    public PP47(String bn,String au,String pr,String date)
    {
        bookname = bn;
        author = au;
        press = pr;
        copyrightdate = date;
    }

    public void setBookname(String bn1)
    {
        bookname = bn1;
    }

    public void setAuthor(String au1)
    {
        author = au1;
    }

    public void setPress(String pr1)
    {
        press = pr1;
    }

    public void setCopyrightdate(String date1)
    {
        copyrightdate = date1;
    }

    public String getBn()
    {
        return bookname;
    }

    public String getAu()
    {
        return author;
    }

    public String getPr()
    {
        return press;
    }

    public String getDate()
    {
        return copyrightdate;
    }

    public String toString()
    {
        return "Bookname: " + bookname + "Author: " + author + "Press: " + press + "Copyrightdate: " + copyrightdate;
    }
}

