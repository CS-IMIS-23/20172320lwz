package src.week5.pp92;

import org.junit.Test;

import static org.junit.Assert.*;

public class SortingTest {

    @Test
    public void selectionSort() {
        Sorting sorting=new Sorting();

        Comparable[] num = {1,4,3,2320};
        sorting.selectionSort(num);
        assertEquals("1 3 4 2320 ",toPrint(num));

        Comparable[] num1 = {1,2,3,4,2320};
        sorting.selectionSort(num1);
        assertEquals("1 2 3 4 2320 ",toPrint(num1));

        Comparable[] num2 = {1,6,3,4,2320};
        sorting.selectionSort(num2);
        assertEquals("1 3 4 6 2320 ",toPrint(num2));

        Comparable[] num3 = {1,1,1,1,2320};
        sorting.selectionSort(num3);
        assertEquals("1 1 1 1 2320 ",toPrint(num3));

        Comparable[] num4 = {2320,2320,2320};
        sorting.selectionSort(num4);
        assertEquals("2320 2320 2320 ",toPrint(num4));

        Comparable[] num5 = {4,4,4,4};
        sorting.selectionSort(num5);
        assertEquals("4 4 4 4 ",toPrint(num5));

        Comparable[] num6 = {"a","b","c","d"};
        sorting.selectionSort(num6);
        assertEquals("a b c d ",toPrint(num6));

        Comparable[] num7 = {6,6,6,6};
        sorting.selectionSort(num7);
        assertEquals("6 6 6 6 ",toPrint(num7));

        Comparable[] num8 = {"b","b","b","b"};
        sorting.selectionSort(num8);
        assertEquals("b b b b ",toPrint(num8));

        Comparable[] num9 = {"b","a","a","b"};
        sorting.selectionSort(num9);
        assertEquals("a a b b ",toPrint(num9));
    }

    public String toPrint(Comparable[] data){
        String result="";
        for (int i=0;i<data.length;i++){
            result+=data[i]+" ";
        }
        return result;
    }
}