package src.week5;

import org.junit.Test;

import static org.junit.Assert.*;

public class SearchingTest {
    @Test
    public void linearSearch() {
    Comparable num[]={1,4,2,3,6,9,12,10,2320};
    assertEquals(true,Searching.linearSearch(num,0,8,1));
    assertEquals(true,Searching.linearSearch(num,0,8,2320));
    assertEquals(false,Searching.linearSearch(num,0,8,5));
    assertEquals(true,Searching.linearSearch(num,0,8,2320));
        assertEquals(true,Searching.linearSearch(num,0,8,1));
        assertEquals(true,Searching.linearSearch(num,0,8,2));
        assertEquals(true,Searching.linearSearch(num,0,8,4));
        assertEquals(true,Searching.linearSearch(num,0,8,3));
        assertEquals(false,Searching.linearSearch(num,0,8,55));
        assertEquals(false,Searching.linearSearch(num,0,8,15));

    }
}