package src.week5.实践;

public class Haxi {
    int num1 = 0;//查找次数
    int num2 = 0;//冲突次数
    LinearNode[] linearNodes=new LinearNode[11];

    public Haxi(int[] a){
        solve(a);
    }

    public void solve(int[] x){
        for (int a = 0;a<x.length;a++){
            LinearNode node=new LinearNode(x[a]);
            int input = x[a]%11;
            LinearNode linearNode = linearNodes[input];

            if (linearNode==null){
                linearNodes[input] = node;
                num1++;
            }
            else {
                LinearNode linearNode1 = linearNodes[input];
                while (linearNode1.getNext()!=null){
                    linearNode1=linearNode1.getNext();
                    num1++;
                }
                linearNode1.setNext(node);
                num2++;
                num1++;
            }
        }
    }

}
