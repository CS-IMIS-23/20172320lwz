package src.week4;


import src.week2.EmptyCollectionException;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

//public abstract class LinkedList<T> implements ListADT<T>, Iterable<T>
//{
//    protected int count;
//    protected LinearNode<T> head, tail;
//    protected int modCount;
//
//
//    public LinkedList()
//    {
//        count = 0;
//        head = tail = null;
//        modCount = 0;
//    }
//
//
//
//    @Override
//    public T removeFirst() throws EmptyCollectionException
//    {
//
//    }
//
//
//    @Override
//    public T removeLast() throws EmptyCollectionException
//    {
//
//    }
//
//
//    @Override
//    public T remove(T targetElement) throws EmptyCollectionException,
//            ElementNotFoundException
//    {
//        if (isEmpty())
//            throw new EmptyCollectionException("LinkedList");
//
//        boolean found = false;
//        LinearNode<T> previous = null;
//        LinearNode<T> current = head;
//
//        while (current != null && !found)
//            if (targetElement.equals(current.getElement()))
//                found = true;
//            else
//            {
//                previous = current;
//                current = current.getNext();
//            }
//
//        if (!found)
//            throw new ElementNotFoundException("LinkedList");
//
//        if (size() == 1)  // only one element in the list
//            head = tail = null;
//        else if (current.equals(head))  // target is at the head
//            head = current.getNext();
//        else if (current.equals(tail))  // target is at the tail
//        {
//            tail = previous;
//            tail.setNext(null);
//        }
//        else  // target is in the middle
//            previous.setNext(current.getNext());
//
//        count--;
//        modCount++;
//
//        return current.getElement();
//    }
//
//
//    @Override
//    public T first() throws EmptyCollectionException
//    {
//        // To be completed as a Programming Project
//    }
//
//
//    @Override
//    public T last() throws EmptyCollectionException
//    {
//        // To be completed as a Programming Project
//    }
//
//
//    @Override
//    public boolean contains(T targetElement) throws
//            EmptyCollectionException
//    {
//        // To be completed as a Programming Project
//    }
//
//
//    @Override
//    public boolean isEmpty()
//    {
//        // To be completed as a Programming Project
//    }
//
//
//    @Override
//    public int size()
//    {
//        // To be completed as a Programming Project
//    }
//
//
//    @Override
//    public String toString()
//    {
//        // To be completed as a Programming Project
//    }
//
//
//    @Override
//    public Iterator<T> iterator()
//    {
//        return new LinkedListIterator();
//    }
//
//
//    private class LinkedListIterator implements Iterator<T>
//    {
//        private int iteratorModCount;  // the number of elements in the collection
//        private LinearNode<T> current;  // the current position
//
//
//        public LinkedListIterator()
//        {
//            current = head;
//            iteratorModCount = modCount;
//        }
//
//
//        @Override
//        public boolean hasNext() throws ConcurrentModificationException
//        {
//            if (iteratorModCount != modCount)
//                throw new ConcurrentModificationException();
//
//            return (current != null);
//        }
//
//
//        @Override
//        public T next() throws ConcurrentModificationException
//        {
//            if (!hasNext())
//                throw new NoSuchElementException();
//
//            T result = current.getElement();
//            current = current.getNext();
//            return result;
//        }
//
//
//        @Override
//        public void remove() throws UnsupportedOperationException
//        {
//            throw new UnsupportedOperationException()  ;
//        }
//    }
//
//}



