package src.week4.PP68;

public class Test {
    public static void main(String[] args) {
        ArrayOrderedList arrayOrderedList =new ArrayOrderedList();
        arrayOrderedList.add(20);
        arrayOrderedList.add(17);
        arrayOrderedList.add(23);
        arrayOrderedList.add(20);
        System.out.println(arrayOrderedList);
        System.out.println("列表头部："+arrayOrderedList.first());
        System.out.println("列表末端："+arrayOrderedList.last());
        System.out.println("元素数目："+arrayOrderedList.size());
        arrayOrderedList.remove(17);
        System.out.println("确定是否存在："+arrayOrderedList.contains(17));
        System.out.println("移除头部："+arrayOrderedList.removeFirst());
        System.out.println("移除尾部："+arrayOrderedList.removeLast());

        ArrayUnorderedList arrayUnorderedList = new ArrayUnorderedList();
        arrayUnorderedList.addToFront(20);
        arrayUnorderedList.addToFront(17);
        arrayUnorderedList.addToFront(2320);
        System.out.println(arrayUnorderedList.toString());
        System.out.println("列表头部："+arrayUnorderedList.first());
        System.out.println("列表尾部："+arrayUnorderedList.last());
        arrayUnorderedList.remove(2320);
        System.out.println("确定是否存在："+arrayUnorderedList.contains(2320));

    }
}
