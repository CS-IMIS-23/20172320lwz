package src.week4.实践;

public class Product implements Comparable<Product> {
    private String name;
    private int value;

    public Product(String name, int value) {
        this.name = name;
        this.value = value;

    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setValue(int value) {
        this.value = value;
    }


    @Override
    public String toString() {
        return "商品：" + name + "价格：" + value;
    }

    @Override
    public int compareTo(Product product) {
        if (name.compareTo(product.getName()) > 0) {
            return 1;
        } else {
            if (name.compareTo(product.getName()) < 0) {
                return -1;
            } else {
                if (value > product.value) {
                    return 1;
                } else {
                    if (value < product.value) {
                        return -1;
                    }
                    else {
                        return 0;
                    }
                }
            }
        }
    }
}



