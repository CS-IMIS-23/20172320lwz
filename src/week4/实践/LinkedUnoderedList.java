package src.week4.实践;

;
public class LinkedUnoderedList<T> extends LinkedList<T>
{

    public LinkedUnoderedList()
    {
        super();
    }


    public void addToFront(T element)
    {
        LinearNode<T> temp = new LinearNode<>(element);
        if (count == 0)
            head = tail = temp;
        else {
            temp.setNext(head);
            head = temp;
        }
        count++;
    }
    public void addToRear(T element) {
        LinearNode<T> temp = new LinearNode<>(element);
        if (count == 0)
            head = tail = temp;
        else {
            tail.setNext(temp);
            tail = temp;
        }
        count++;
    }
    public void addAfter(T element, T target)
    {
        LinearNode<T> node1 = new LinearNode<>(element);
        LinearNode<T> current = head;
        while(current.getElement() != target)
            current = current.getNext();
        node1.setNext(current.getNext());
        current.setNext(node1);
        if(node1.getNext() == null)
            tail = node1;
        count++;
    }
}

