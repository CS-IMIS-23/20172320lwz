package src.week4.PP617;

import src.week4.PP617.LinkedOrderedList;

public class test3 {
    public static void main(String[] args) {
        Course2 c1 = new Course2("java",111,"Java Study");
        Course2 c2 = new Course2("math",101,"math class");
        Course2 c3 = new Course2("English",112,"English Study");
        LinkedOrderedList linkedOrderedList = new LinkedOrderedList();
        linkedOrderedList.addCourse(c1);
        linkedOrderedList.addCourse(c3);
        linkedOrderedList.addCourse(c2);
        System.out.println(linkedOrderedList);
    }
}
