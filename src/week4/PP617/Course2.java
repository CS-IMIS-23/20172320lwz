package src.week4.PP617;

import java.io.Serializable;

public class Course2 implements Serializable

{
    private String prefix;
    private int number;
    private String title;
    private String grade;
    public Course2(String prefix, int number, String title, String grade)
    {
        this.prefix = prefix;
        this.number = number;
        this.title = title;
        if (grade == null) {
            this.grade = "";
        } else {
            this.grade = grade;
        }
    }
    public Course2(String prefix, int number, String title)
    {
        this(prefix, number, title, "");
    }
    public String getPrefix()
    {
        return prefix;
    }
    public int getNumber()
    {
        return number;
    }
    public String getTitle()
    {
        return title;
    }
    public String getGrade()
    {
        return grade;
    }
    public void setGrade(String grade)
    {
        this.grade = grade;
    }
    public boolean taken() {
        return !grade.equals("");
    }
    @Override
    public boolean equals(Object other)
    {
        boolean result = false;
        if (other instanceof src.week4.Course)
        {
            Course2 otherCourse = (Course2) other;
            if (prefix.equals(otherCourse.getPrefix()) &&
                    number == otherCourse.getNumber())
                result = true;
        }
        return result;
    }
    @Override
    public String toString()
    {
        String result = prefix + " " + number + ": " + title;
        if (!grade.equals(""))
            result += "  [" + grade + "]";
        return result;
    }
    public int campareTo(Course2 course2){
        if (title.compareTo(course2.getTitle())>0){
            return 1;
        }
        else {
            if (title.compareTo(course2.getTitle())<0){
                return -1;
            }
            else {
                if (number>course2.getNumber()) {
                    return 1;
                } else {
                    return -1;
                }
            }
        }
    }
}