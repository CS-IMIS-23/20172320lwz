package src.week4.实验一;
import java.io.*;
import java.util.StringTokenizer;

public class Test {
    public static void main(String[] args) throws IOException {
      Exam integer = new Exam(20);
        Exam Head = integer;
        int nLiWenZhou = 0;
        Exam integer1 = new Exam(17);
        Exam integer2 = new Exam(23);
        Exam integer3 = new Exam(20);
        Exam integer4 = new Exam(20);
        Exam integer5 = new Exam(18);
        Exam integer6 = new Exam(9);
        Exam integer7 = new Exam(28);
        Exam integer8 = new Exam(12);
        Exam integer9 = new Exam(28);
        Exam integer10 = new Exam(40);

        InsertNode(Head,integer1);
        InsertNode(Head,integer2);
        InsertNode(Head,integer3);
        InsertNode(Head,integer4);
        InsertNode(Head,integer5);
        InsertNode(Head,integer6);
        InsertNode(Head,integer7);
        InsertNode(Head,integer8);
        InsertNode(Head,integer9);
        InsertNode(Head,integer10);
        PrintLinkedList(Head);
        nLiWenZhou = Size(Head);
        System.out.println(" ");
        System.out.println("元素数目："+nLiWenZhou);

        //读取1 2
        //实验二
        //读取文件中的1 2
        File file =new File("D://wz","Fn.txt");
//        Reader reader = new FileReader(file);
//        BufferedReader bufferedReader = new BufferedReader(reader);
//        StringTokenizer stringTokenizer= new StringTokenizer(bufferedReader.readLine());
//        int number = stringTokenizer.countTokens();
        Reader reader = null;
        try {
            reader = new InputStreamReader(new FileInputStream(file));
            int temp;
            while ((temp = reader.read())!= -1){
                if (((char)temp)!='\r'){
                    System.out.println("文件中的元素"+(char)temp);
                }
            }
            reader.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
       //插入1
        Exam num1 = new Exam(1);
        Insert(Head,5,num1);
        System.out.println("插入1：");
        PrintLinkedList(Head);
        nLiWenZhou = Size(Head);
        System.out.println("插入1后数目："+nLiWenZhou);
        //插入2
        Exam num2 = new Exam(2);
        Insert(Head,0,num2);
        System.out.println("插入2：");
        PrintLinkedList(Head);
        nLiWenZhou = Size(Head);
        System.out.println("插入2后数目："+nLiWenZhou);
        //删除1
        DeleteNode(Head,num1);
        System.out.println("删除1：");
        PrintLinkedList(Head);
        nLiWenZhou= Size(Head);
        System.out.println("删除1后数目："+nLiWenZhou);
        //排序
        selectSorting(Head);
        System.out.println("排序后：");
        PrintLinkedList(Head);
        nLiWenZhou = Size(Head);
        System.out.println("排序后总数："+nLiWenZhou);
    }


    //输出
    public static void PrintLinkedList(Exam Head) {
        Exam node = Head;
        while (node != null) {
            System.out.println(node.number);
            node = node.next;
        }
    }

    //插入
    public static void InsertNode(Exam Head, Exam node) {
        Exam temp = Head;
        while (temp.next != null) {
            temp = temp.next;
        }
        temp.next = node;
    }
    //在任意位置插入
    public static void Insert(Exam Head,int x,Exam node){
        Exam point = Head;
        if (x!=0){
            for (int a = 0;a<x-2;a++){
                point=point.next;}
                node.next=point.next;
                point.next=node;

        }
        else {
            node.next=Head;
            Head=node;
        }
    }

    //删除
    public static void DeleteNode(Exam Head, Exam node) {
        Exam PreNode = Head, CurrentNode = Head;
        while (CurrentNode != null) {
            if (CurrentNode.number != node.number) {
                PreNode = CurrentNode;
                CurrentNode = CurrentNode.next;
            } else {
                break;
            }
        }
        PreNode.next = CurrentNode.next;
    }

    //排序
    public static Exam selectSorting(Exam Head) {
        Exam PreNode = Head, CurrentNode = null;
        if (Head == null || Head.next == null) {
            return Head;
        }
        while (PreNode.next != CurrentNode){
            while (PreNode.next != CurrentNode) {
                if (PreNode.number > PreNode.next.number) {
                    int temp = PreNode.number;
                    PreNode.number = PreNode.next.number;
                    PreNode.next.number = temp;
                }
                PreNode = PreNode.next;
            }
            CurrentNode = PreNode;
            PreNode = Head;
        }
        return Head;
    }
    public static int Size(Exam Head){
        Exam node = Head;
        int count = 0;
        while (node!=null){
            count=count+1;
            node=node.next;
        }
        return count;
    }
}




