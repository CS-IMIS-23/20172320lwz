package src.week8.实验;

import src.week6.BinaryTreeNode;
import src.week6.LinkedBinaryTree;

public class LinkedBinaryTreeTest {
    public static void main(String[] args) {
        BinaryTreeNode a =new BinaryTreeNode(1);
        BinaryTreeNode b =new BinaryTreeNode(2);
        BinaryTreeNode c = new BinaryTreeNode(3);
        BinaryTreeNode d=new BinaryTreeNode(4);
        BinaryTreeNode e=new BinaryTreeNode(5);

        LinkedBinaryTree A = new LinkedBinaryTree(a.getElement());
        LinkedBinaryTree B=new LinkedBinaryTree(b.getElement());
        LinkedBinaryTree C=new LinkedBinaryTree(d.getElement(),A,B);
        LinkedBinaryTree D=new LinkedBinaryTree(e.getElement(),C,B);

        LinkedBinaryTree linkedBinaryTree=new LinkedBinaryTree(10,C,D);


        System.out.println("输出数："+linkedBinaryTree.toString());
        System.out.println("输出右子树："+linkedBinaryTree.getRight());
        System.out.println("是否含有数字10："+linkedBinaryTree.contains(10));
        System.out.println("前序遍历：");
        linkedBinaryTree.toPreString();
        System.out.println("\n后序输出：");
        linkedBinaryTree.toPostString();

    }
}
