package src.week8.实验;



import java.util.Iterator;
import java.util.Scanner;

public class ITStest {
    public static void main(String[] args) {
      Scanner scanner=new Scanner(System.in);
      InfixToSuffix infixToSuffix = new InfixToSuffix();
      LinkedBinaryTree tree=new LinkedBinaryTree();
      String string;
        System.out.println("请输入中缀表达式：");
        string=scanner.nextLine();
        tree.root=infixToSuffix.BuildTree(string);
        System.out.println(tree.toString());

        String string1="";
        Iterator iterator=tree.iteratorPostOrder();
        while (iterator.hasNext()){
            string1+=iterator.next()+" ";
        }
        System.out.println(string1);

        PostfixEvaluator postfixEvaluator=new PostfixEvaluator();
        int result=postfixEvaluator.evaluate(string1);
        System.out.println("计算结果："+result);

    }
}
