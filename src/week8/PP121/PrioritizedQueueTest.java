package src.week8.PP121;

import src.week8.PriorityQueue;

public class PrioritizedQueueTest {
    public static void main(String[] args) {
        PriorityQueue queue = new PriorityQueue();
        queue.addElement(1,1);
        queue.addElement(9,2);
        queue.addElement(3,3);
        queue.addElement(29,4);

        String string = "";
        for (int i=0;i<queue.getNum();i++){
            string+=queue.removeNext()+" ";
        }
        System.out.println(string);
    }
}
