package src.week8.PP129;

import src.week8.jsjf.LinkedHeap;

public class LinkedHeapTest {
    public static void main(String[] args) {
        LinkedHeap linkedHeap = new LinkedHeap();
        linkedHeap.addElement(1);
        linkedHeap.addElement(4);
        linkedHeap.addElement(6);
        linkedHeap.addElement(23);
        linkedHeap.addElement(12);
        System.out.println(linkedHeap);
        System.out.println("最小值："+linkedHeap.findMin());
        linkedHeap.removeMin();
        System.out.println("删除最小值后："+linkedHeap);
    }
}
