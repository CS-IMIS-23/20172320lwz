package src.week8.PP128;

import src.week8.jsjf.ArrayHeap;

public class ArrayHeapTest {
    public static void main(String[] args) {
        ArrayHeap arrayHeap = new ArrayHeap();
        arrayHeap.addElement(1);
        arrayHeap.addElement(4);
        arrayHeap.addElement(6);
        arrayHeap.addElement(23);
        arrayHeap.addElement(12);
        System.out.println(arrayHeap);
        System.out.println("最小值："+arrayHeap.findMin());
        arrayHeap.removeMin();
        System.out.println("删除最小值后："+arrayHeap);
    }
}
