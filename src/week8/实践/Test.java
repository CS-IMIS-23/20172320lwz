package src.week8.实践;

import src.week8.jsjf.HeapSort;

public class Test {
    public static void main(String[] args) {
      int[] number={36,30,18,40,32,45,22,50};
      MaxArrayHeap max=new MaxArrayHeap();
      for (int n=0;n<number.length;n++){
          max.addElement(number[n]);
      }
        System.out.println("层序遍历："+max.toLevelString());
        System.out.println("每轮排序输出：");
      for (int x=0;x<number.length;x++){
          number[x]= (int) max.removeMax();
          int n = x+1;
          System.out.println("第"+n+"次排序"+max);
      }
        System.out.println("排序:");
        String string="";
      for (int i=0;i<number.length;i++){
           string+=number[i]+" ";
      }
        System.out.println(string);

    }
}
