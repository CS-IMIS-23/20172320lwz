package src.week8.实践;

import src.week8.jsjf.ArrayBinaryTree;
import src.week8.jsjf.ArrayHeap;
import src.week8.jsjf.exceptions.EmptyCollectionException;

public class MaxArrayHeap<T> extends ArrayBinaryTree<T> implements HeapADT<T> {
    public MaxArrayHeap(){
        super();
    }

    @Override
    public void addElement(T obj) {
        if (count == tree.length)
            expandCapacity();

        tree[count] = obj;
        count++;
        modCount++;

        if (count > 1)
            heapifyAdd();
    }
    private void heapifyAdd()
    {
        T temp;
        int next = count - 1;

        temp = tree[next];

        while ((next != 0) &&
                (((Comparable)temp).compareTo(tree[(next-1)/2]) < 0))
        {

            tree[next] = tree[(next-1)/2];
            next = (next-1)/2;
        }

        tree[next] = temp;
    }
    @Override
    public T removeMax() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("ArrayHeap");

        T maxElement = tree[0];
        tree[0] = tree[count-1];
        heapifyRemove();
        count--;
        modCount--;
        tree[count]=null;

        return maxElement;
    }
    private void heapifyRemove()
    {
        T temp;
        int node = 0;
        int left = 1;
        int right = 2;
        int next;

        if ((tree[left] == null) && (tree[right] == null))
            next = count;
        else if (tree[right] == null)
            next = left;
        else if (((Comparable)tree[left]).compareTo(tree[right]) > 0)
            next = left;
        else
            next = right;
        temp = tree[node];

        while ((next < count) &&
                (((Comparable)tree[next]).compareTo(temp) >0))
        {
            tree[node] = tree[next];
            node = next;
            left = 2 * node + 1;
            right = 2 * (node + 1);
            if ((tree[left] == null) && (tree[right] == null))
                next = count;
            else if (tree[right] == null)
                next = left;
            else if (((Comparable)tree[left]).compareTo(tree[right]) > 0)
                next = left;
            else
                next = right;
        }
        tree[node] = temp;
    }

    @Override
    public T findMax() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("ArrayHeap");

        return tree[0];
    }

    @Override
    public boolean contains(Object targetElement) {
        return false;
    }

    public String toLevelString() {
        String string="";
        int x=0;
        while (tree[x]!=null){
            string+=tree[x]+" ";
            x++;
        }
        return string;
    }
}
