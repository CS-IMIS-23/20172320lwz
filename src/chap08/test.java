package chap8;
/*
pp8_5.java                  Author:zhaoxiaohai
计算并且输出一组整数的平均值和标准方差。假设输入不超过50个值。以浮点数计算均值和标准方差。
 */
import java.util.Scanner;

public class test {
    public static void main(String[] args) {
        final int LIMIT=50;
        double mean,sd,add1=0,add2=0;
        int n=0,input;
        String another="y";
        int[]statistic=new int[LIMIT];

        Scanner scan=new Scanner(System.in);
        System.out.print("请您输入一个整数： ");
        input=scan.nextInt();

        while (another.equalsIgnoreCase("y"))
        {
            statistic [n]=input;
            n++;
            System.out.print("是否继续输入（y/n）?: ");
            another=scan.next();
            if(another.equalsIgnoreCase("y"))
            {
                System.out.print("请您输入下一个整数： ");
                input=scan.nextInt();
            }
        }
        for (int n1=0;n1<n;n1++)
            add1+=statistic[n1];
        mean=add1/n;
        for (int n2=0;n2<n;n2++)
            add2+=Math.pow(statistic[n2]-mean,2);
        sd=Math.sqrt(add2);

        for (int n3=0;n3<n;n3++)
            System.out.print(statistic[n3]+" ");
        System.out.println();
        System.out.println("以上这组数的平均值是： "+mean+", 标准方差为: "+sd);
    }

}

