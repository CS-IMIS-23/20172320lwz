package src.chap08;
import java.util.Scanner;
public class PP85 {
    public static void main(String[] args) {
        final int limit = 51;
        int num;
        int n = 0;//输入数字的个数
        double x = 0;
        double y = 0;
        double mean, sd;
        Scanner scan = new Scanner(System.in);
        int[] count = new int[limit];

        System.out.println("输入一个整数：");
        num = scan.nextInt();

        String another = "y";
        while (another.equalsIgnoreCase("y")) {
            count[n]=num;
            n++;
            System.out.println("是否继续（y/n): ");
            another = scan.next();
            if (another.equalsIgnoreCase("y")) {
                System.out.println("输入一个整数：");
                num = scan.nextInt();
            }
        }
        for (int i = 0; i <n; i++) {
            System.out.println(count[i] + " ");
            x = x + count[i];
            mean = x / n;
            y = y + Math.pow(count[i] - mean, 2);
            sd = Math.sqrt(y);

            if (i == n-1) {
                System.out.println();
                System.out.println("平均数：" + mean + "\n" + "方差：" + sd);
            }
        }
    }
}
