package src.chap08;

public class Grade
{
    private String name;
    private int lowerBound;

    public Grade(String grade, int cutoff)
    {
        name=grade;
        lowerBound=cutoff;
    }
    //-----------------------------------------------------
    //Returns a string representation of this garde.
    //------------------------------------------------------

    public String toString()
    {
     return name + "\t"+lowerBound;
    }
    //---------------------------------------------------------
    //Name mutator
    //---------------------------------------------------------
    public void setLowerBound(int cutoff)
    {
        lowerBound=cutoff;
    }
    //-----------------------------------------------------------
    //Name accessor.
    //-----------------------------------------------------------

    public String getName() {
        return name;
    }
    //----------------------------------------------------
    //Lower bound accessor.
    //----------------------------------------------------
    public int getLowerBound()
    {
        return lowerBound;
    }
}
