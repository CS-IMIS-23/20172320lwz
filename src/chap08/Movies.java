package src.chap08;

public class Movies {
    public static void main(String[] args) {
        DVDCollection movies = new DVDCollection();

        movies.addDVD("The Godfather","Francis Ford Coppola",1972,24.95,true);
        movies.addDVD("Districy 9","Neill Blomkamp",2009,19.95,false);
        movies.addDVD("SwordArt Online","WZ",2012,100.00,true);
        System.out.println(movies);

        movies.addDVD("Fate StayNight","WZ",2014,99.99,false);
        movies.addDVD("Garo Kami no Kiba","WZ",2018,99.99,true);
        System.out.println(movies);
    }
}
