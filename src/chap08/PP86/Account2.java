package src.chap08.PP86;

import src.chap08.PP86.Account;

public class Account2 {
    Account[] accounts;
    private int i;

    public Account2(){
        accounts=new Account[30];
        i=0;
    }
    public void setAccounts(String owner,long acc,double initial){
        accounts[i]=new Account(owner,acc,initial);
        i++;
    }//建立账户

    public void setMoney(Account account,double balence) {
        for (int n = 0; n < 30; n++)
            if (balence < 0) {
                System.out.println("存钱失败！");
            }
        double d = balence + account.getBalance();
        account.setBalance(d);
        System.out.println("已存入" + balence + "元,余额为" + d);
    }//存钱

    public void getMoney(Account account,double balence) {
        double d = account.getBalance();
        account.setBalance(d - balence);
        if (balence > d)
            System.out.println("余额不足！");
        else
            System.out.println("取出" + balence + "元,余额" + account.getBalance());
    }//取钱

    public void addInterest(){
        int i=0;
        while (i<30){
            if (accounts[i]!=null)
                accounts[i].addInterest();
            i++;
        }//利息
    }

}