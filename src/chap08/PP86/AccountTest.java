package src.chap08.PP86;

import src.chap08.PP86.Account2;

public class AccountTest {
    public static void main(String[] args) {
        Account2 n=new Account2();
        n.setAccounts("wz",1,0);
        n.setAccounts("nc",2,0);
        n.setAccounts("cwh",3,0);
        n.setAccounts("hxx",4,0);
        System.out.println(n.accounts[0]);
        System.out.println(n.accounts[1]);
        System.out.println(n.accounts[2]);
        System.out.println(n.accounts[3]);
        System.out.println();

        n.setMoney(n.accounts[0],10000.0);
        n.setMoney(n.accounts[1],9000.0);
        n.setMoney(n.accounts[2],8000.0);
        n.setMoney(n.accounts[3],1000.0);
        System.out.println();


        n.getMoney(n.accounts[0],5000.0);
        n.getMoney(n.accounts[1],2000.0);
        n.getMoney(n.accounts[2],0);
        n.getMoney(n.accounts[3],2000.0);
        System.out.println();


        n.addInterest();
        System.out.println("增加利息后"+n.accounts[0]);
        System.out.println("增加利息后"+n.accounts[1]);
        System.out.println("增加利息后"+n.accounts[2]);
        System.out.println("增加利息后"+n.accounts[3]);
    }
}
