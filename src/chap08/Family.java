package src.chap08;

public class Family {
    private String[] members;

    public Family(String ... names){
        members=names;
    }

    //-------------------------------------------------------------
    //Returns a string representation of this family.
    //-------------------------------------------------------------

    @Override
    public String toString() {
        String result = "";
        for (String name : members)
            result+=name+"\n";

        return result;
    }
}
