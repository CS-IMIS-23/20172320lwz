package src.chap08;
import java.text.NumberFormat;

public class DVDCollection {
private DVD[] colletion;
private int count;
private double totalCost;

//--------------------------------------------------
//Constructor: Creates an initially empty collection
//---------------------------------------------------
public DVDCollection(){
    colletion=new DVD[100];
    count=0;
    totalCost = 0.0;
    }

    //-----------------------------------------------------------------
    //Adds a DVD to the collection, increasing the size of the
    //collection array if necessary.
    //-------------------------------------------------------------------
    public void addDVD(String title, String director, int year, double cost, boolean bluray)
    {
        if (count==colletion.length)
           increaseSize();

        colletion[count]=new DVD(title,director,year,cost,bluray);
        totalCost+=cost;
        count++;
    }

    //--------------------------------------------------------------------
    //Returns a report describing the DVD collection
    //----------------------------------------------------------------------
    public String toString()
    {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();

        String report = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
        report+="My DVD collection\n\n";

        report+="Number of DVDs: "+count+"\n";
        report+="Total cost: "+fmt.format(totalCost)+"\n";
        report+="Average cost: "+fmt.format(totalCost/count);

        report+="\n\nDVD List:\n\n";

        for (int dvd = 0;dvd<count;dvd++)
            report+=colletion[dvd].toString()+"\n";

        return report;
    }

    private void increaseSize()
    {
        DVD[] temp = new DVD[colletion.length*2];

        for (int dvd = 0;dvd<colletion.length;dvd++)
            temp[dvd]=colletion[dvd];
    }
}
