package src.week7;

public class LinkedBinarySeachTreeTest {
    public static void main(String[] args) throws EmptyCollectionException{
        LinkedBinarySearchTree linkedBinarySearchTree = new LinkedBinarySearchTree(5);
        linkedBinarySearchTree.addElement(1);
        linkedBinarySearchTree.addElement(21);
        linkedBinarySearchTree.addElement(32);
        linkedBinarySearchTree.addElement(14);
        linkedBinarySearchTree.addElement(5);

        System.out.println(linkedBinarySearchTree.toString());


        System.out.println("最大元素"+linkedBinarySearchTree.findMax());
        System.out.println("最小元素"+linkedBinarySearchTree.findMin());

        linkedBinarySearchTree.removeMax();
        System.out.println("删除最大元素后："+linkedBinarySearchTree.toString());

    }
}
