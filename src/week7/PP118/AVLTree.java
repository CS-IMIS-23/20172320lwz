package src.week7.PP118;

public class AVLTree<T extends Comparable<T>> {
    private AVLTreeNode<T> Root;


    class AVLTreeNode<T extends Comparable<T>> {
        T element;
        int height;
        AVLTreeNode<T> left;
        AVLTreeNode<T> right;

        public AVLTreeNode(T element, AVLTreeNode<T> left, AVLTreeNode<T> right) {
            this.element =element;
            this.left = left;
            this.right = right;
            this.height = 0;
        }
        public T getElement() {
            return element;
        }
        public AVLTreeNode<T> getRight() {
            return right;
        }
        public AVLTreeNode<T> getLeft() {
            return left;
        }
        public void setElement(T element) {
            this.element = element;
        }
        public void setLeft(AVLTreeNode<T> left) {
            this.left = left;
        }
        public void setRight(AVLTreeNode<T> right) {
            this.right = right;
        }
        public boolean internalNode(){
            if(left == null && right == null) {
                return false;
            } else {
                return true;
            }
        }
    }
    private int height(AVLTreeNode<T> tree) {
        if (tree != null) {
            return tree.height;
        }

        return 0;
    }

    public int height() {
        return height(Root);
    }
    private int max(int a, int b) {
        return a>b ? a : b;
    }
    private AVLTreeNode<T> leftLeftRotation(AVLTreeNode<T> k2) {
        AVLTreeNode<T> k1;

        k1 = k2.left;
        k2.left = k1.right;
        k1.right = k2;

        k2.height = max( height(k2.left), height(k2.right)) + 1;
        k1.height = max( height(k1.left), k2.height) + 1;

        return k1;
    }
    private AVLTreeNode<T> rightRightRotation(AVLTreeNode<T> k1) {
        AVLTreeNode<T> k2;

        k2 = k1.right;
        k1.right = k2.left;
        k2.left = k1;

        k1.height = max( height(k1.left), height(k1.right)) + 1;
        k2.height = max( height(k2.right), k1.height) + 1;

        return k2;
    }
    private AVLTreeNode<T> leftRightRotation(AVLTreeNode<T> k3) {
        k3.left = rightRightRotation(k3.left);

        return leftLeftRotation(k3);
    }
    private AVLTreeNode<T> rightLeftRotation(AVLTreeNode<T> k1) {
        k1.right = leftLeftRotation(k1.right);

        return rightRightRotation(k1);
    }
    private AVLTreeNode<T> minimum(AVLTreeNode<T> tree) {
        if (tree == null)
            return null;

        while(tree.left != null)
            tree = tree.left;
        return tree;
          }

    public T minimum() {
       AVLTreeNode<T> p = minimum(Root);
        if (p != null)
           return p.element;

              return null;
           }

    private AVLTreeNode<T> maximum(AVLTreeNode<T> tree) {
        if (tree == null) {
            return null;
        }

        while(tree.right != null) {
            tree = tree.right;
        }
        return tree;
             }
             public T maximum() {
        AVLTreeNode<T> p = maximum(Root);
        if (p != null) {
            return p.element;
        }

        return null;
             }
    private AVLTreeNode<T> search(AVLTreeNode<T> x, T key) {
        if (x==null) {
            return x;
        }

        int cmp = key.compareTo(x.element);
        if (cmp < 0) {
            return search(x.left, key);
        } else if (cmp > 0) {
            return search(x.right, key);
        } else {
            return x;
        }
            }

             public AVLTreeNode<T> search(T key) {
                return search(Root, key);
             }

    private AVLTreeNode<T> insert(AVLTreeNode<T> tree, T key) {
        if (tree == null) {

            tree = new AVLTreeNode<T>(key, null, null);
            if (tree==null) {
                System.out.println("ERROR: create avltree node failed!");
                return null;
            }
        } else {
            int cmp = key.compareTo(tree.element);

            if (cmp < 0) {
                tree.left = insert(tree.left, key);

                if (height(tree.left) - height(tree.right) == 2) {
                    if (key.compareTo(tree.left.element) < 0) {
                        tree = leftLeftRotation(tree);
                    } else {
                        tree = leftRightRotation(tree);
                    }
                }
            } else if (cmp > 0) {
                tree.right = insert(tree.right, key);
                if (height(tree.right) - height(tree.left) == 2) {
                    if (key.compareTo(tree.right.element) > 0) {
                        tree = rightRightRotation(tree);
                    } else {
                        tree = rightLeftRotation(tree);
                    }
                }
            } else {
                System.out.println("添加失败：不允许添加相同的节点！");
            }
        }

        tree.height = max( height(tree.left), height(tree.right)) + 1;

        return tree;
    }

    public void insert(T key) {
        Root = insert(Root, key);
    }
    private AVLTreeNode<T> remove(AVLTreeNode<T> tree, AVLTreeNode<T> z) {
        if (tree==null || z==null)
            return null;

        int cmp = z.element.compareTo(tree.element);
        if (cmp < 0) {
            tree.left = remove(tree.left, z);
            if (height(tree.right) - height(tree.left) == 2) {
                AVLTreeNode<T> r =  tree.right;
                if (height(r.left) > height(r.right))
                    tree = rightLeftRotation(tree);
                else
                    tree = rightRightRotation(tree);
            }
        } else if (cmp > 0) {
            tree.right = remove(tree.right, z);

            if (height(tree.left) - height(tree.right) == 2) {
                AVLTreeNode<T> l =  tree.left;
                if (height(l.right) > height(l.left))
                    tree = leftRightRotation(tree);
                else
                    tree = leftLeftRotation(tree);
            }
        } else {
            if ((tree.left!=null) && (tree.right!=null)) {
                if (height(tree.left) > height(tree.right)) {

                    AVLTreeNode<T> max = maximum(tree.left);
                    tree.element = max.element;
                    tree.left = remove(tree.left, max);
                } else {
                    AVLTreeNode<T> min = maximum(tree.right);
                    tree.element= min.element;
                    tree.right = remove(tree.right, min);
                }
            } else {
                AVLTreeNode<T> tmp = tree;
                tree = (tree.left!=null) ? tree.left : tree.right;
                tmp = null;
            }
        }

        return tree;
    }

    public void remove(T key) {
        AVLTreeNode<T> z;

        if ((z = search(Root, key)) != null) {
            Root = remove(Root, z);
        }
    }
    private void preOrder(AVLTreeNode<T> tree) {
        if(tree != null) {
            System.out.print(tree.element+" ");
            preOrder(tree.left);
            preOrder(tree.right);
            }
    }
    public void preOrder(){
        preOrder(Root);
    }
    private void inOrder(AVLTreeNode<T> tree) {
        if(tree != null)
        {
            inOrder(tree.left);
            System.out.print(tree.element+" ");
            inOrder(tree.right);
            }
            }
    public void inOrder() {
        inOrder(Root);
             }
    private void postOrder(AVLTreeNode<T> tree) {
        if(tree != null) {
            postOrder(tree.left);
            postOrder(tree.right);
            System.out.print(tree.element+" ");
 }
 }

    public void postOrder() {
        postOrder(Root); }

}
