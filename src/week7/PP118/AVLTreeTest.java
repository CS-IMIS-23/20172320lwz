package src.week7.PP118;

public class AVLTreeTest {
    public static void main(String[] args) {
        int i;
        int num[]={3,2,1,4,5,6,7,16,15,14,13,12,11,10,8,9};
        AVLTree<Integer> tree = new AVLTree<Integer>();
        System.out.println("添加：");
        for (i=0;i<num.length;i++){
            System.out.println(num[i]);
            tree.insert(num[i]);
        }

        System.out.println("前序遍历：");
        tree.preOrder();

        System.out.println("\n中序遍历：");
        tree.inOrder();

        System.out.println("\n后序遍历：");
        tree.postOrder();

        System.out.println("\n高度:"+tree.height());


        tree.remove(8);
        System.out.println("\n删除后：");
        tree.preOrder();

    }
}
