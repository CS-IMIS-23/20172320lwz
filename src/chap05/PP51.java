import java.util.Scanner;

public class PP51
{
public static void main(String[] args)
{
final int standard = 1582;
int year;

Scanner scan = new Scanner(System.in);
System.out.println("Please enter the year: " );
year = scan.nextInt();

if (year < standard )
System.out.println("Error");
else
if (year%4==0&&year%100 !=0 ||year%100==0 ||year%400==0 )
System.out.println("This year is a leap year.");
else
System.out.println("This year isn't a leap year.");
}
} 
