package src.cn.edu.besti.cs1723.LWZ2320;

public class SearchingTest {
    public static void main(String[] args) {
        int[] num={1,2,3,4,5,6,7,8,9};


        System.out.println("顺序查找："+Searching.SequenceSearch(num,5,9));


        System.out.println("插值查找："+Searching.InsertSearch(num,5,0,8));
        System.out.println("插值查找："+Searching.InsertSearch(num,1,0,8));

        System.out.println("斐波那契查找:"+Searching.FibonacciSearch(num,9,1));
        System.out.println("斐波那契查找:"+Searching.FibonacciSearch(num,9,5));


        ;
    }
}
