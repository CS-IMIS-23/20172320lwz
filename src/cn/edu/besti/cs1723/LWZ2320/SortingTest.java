package src.cn.edu.besti.cs1723.LWZ2320;

import org.junit.Test;

import static org.junit.Assert.*;

public class SortingTest {

    @Test
    public void shellsort() {
        Comparable num[]={1,3,2,55,13,22,2320};
        System.out.println("希尔排序:");
        Sorting.shellsort(num);
        for (int i =0;i<num.length;i++){
            System.out.println(num[i]+" ");
        }
    }

    @Test
    public void heapSort() {
        Comparable num[]={1,3,2,55,13,22,2320};
        System.out.println("堆排序:");
        Sorting.heapSort(num);
        for (int i =0;i<num.length;i++){
            System.out.println(num[i]+" ");
        }
    }

    @Test
    public void binaryTreeSort() {
        Comparable num[]={1,3,2,55,13,22,2320};
        System.out.println("二叉树排序:");
        Sorting.binaryTreeSort(num);
        for (int i =0;i<num.length;i++){
            System.out.println(num[i]+" ");
        }
    }
}