//This is a TempConverter program.
import java.util.Scanner;
public class PP25
{
public static void main(String[] args)
{
final int base = 32;
final double conversion_factor = 5.0/9.0;

int fahrenheit;
double celsius;

Scanner scan = new Scanner(System.in);

System.out.println("Enter the number of Fahrenheit:");
fahrenheit = scan.nextInt();

celsius = conversion_factor*(fahrenheit-base);
System.out.println("Celsius:"+celsius);
}
}
