package src.week9.jsjf.PP157;

public class Test {
    public static void main(String[] args) {
        NetWork netWork=new NetWork();
        netWork.addCity("北京");
        netWork.addCity("上海");
        netWork.addCity("广州");
        netWork.addCityEdge("北京","上海",1500);
        netWork.addCityEdge("广州","上海",120);
        netWork.addCityEdge("上海","广州",210);
        netWork.addCityEdge("广州","北京",190);
        System.out.println(netWork.toString());
        System.out.println("\n从北京到上海的最短路径："+netWork.shortestPathLength("北京","上海"));
        System.out.println("\n从北京到上海的最便宜路径："+netWork.shortestPathWeight("北京","上海"));
    }
}
