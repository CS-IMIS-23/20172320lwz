package src.week9.jsjf.PP157;

import src.week9.jsjf.GraphADT;

public interface NetWorkADT<T> extends GraphADT<T> {
    public void addEdge(T vertex1, T vertex2, double weight);
    public double shortestPathWeight(T vertex1, T vertex2);
}
