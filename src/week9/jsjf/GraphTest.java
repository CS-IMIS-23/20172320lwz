package src.week9.jsjf;

public class GraphTest {
    public static void main(String[] args) {
        Graph graph=new Graph();
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addVertex(3);

        graph.addEdge(1,4);
        graph.addEdge(1,5);
        graph.addEdge(2,3);
        graph.addEdge(2,4);
        graph.addEdge(3,8);
        graph.addEdge(3,1);
        System.out.println(graph);
        System.out.println(graph.isConnected());
    }
}
