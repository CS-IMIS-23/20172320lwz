package src.test5;


import src.test5.Operand;

public class Questions {

    private Operand opd;



    public Questions(){

        opd = new Operand();

    }



    //生成四则运算

    public String getAdd(){

        return opd.getOp1() + " + " + opd.getOp2();

    }



    public String getSub(){

        return opd.getOp1() + " - " + opd.getOp2();

    }



    public String getMulti(){

        return opd.getOp1() + " * " + opd.getOp2();

    }



    public String getDiv(){

        return opd.getOp1() + " / " + opd.getOp2();

    }

}

