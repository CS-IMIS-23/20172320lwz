package src.test5;

import java.util.Random;

public class Operand {

    private Random rnd1 = new Random();

    private Random rnd2 = new Random();

    private Random rnd3 = new Random();

    private RationalNumber b;

    private String  a, op1, op2;

    private int c, d;



    public Operand(){



    }

    //随机获取操作数，可能为整数可能为分数

    public String getOp1(){

        if (rnd3.nextInt(2) == 0){

            op1 = getA();

        }

        else

            op1 = getB().toString();

        return op1;

    }



    public String getOp2(){

        if (rnd3.nextInt(2) == 0){

            op2 = getA();

        }

        else

            op2 = getB().toString();

        return op2;

    }

    //随机获取一个整数并将它转化为String类型

    private String  getA() {

        a = String.valueOf(rnd1.nextInt(10) + 1);

        return a;

    }

    //随机获取一个真分数

    private RationalNumber getB(){

        while (true) {

            c = rnd1.nextInt(10) + 1;

            d = rnd2.nextInt(10) + 1;

            b = new RationalNumber(c, d);

            if (c < d){

                break;

            }

        }

        return b;

    }

}

